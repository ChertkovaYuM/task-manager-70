package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;
import ru.tsc.chertkova.tm.service.TaskService;
import ru.tsc.chertkova.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.chertkova.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private TaskService TaskService;

    @Override
    @WebMethod
    @GetMapping("/create")
    public void create() {
        TaskService.create(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDTO> findAll() {
        return TaskService
                .findAll(UserUtil.getUserId())
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDTO findById(@NotNull @PathVariable("id") final String id) {
        return TaskService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return TaskService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDTO save(
            @NotNull
            @WebParam(name = "TaskDTO")
            @RequestBody final TaskDTO TaskDTO
    ) {
        return TaskService.save(UserUtil.getUserId(), TaskDTO);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "TaskDTO")
            @RequestBody final TaskDTO TaskDTO
    ) {
        TaskService.remove(UserUtil.getUserId(), TaskDTO);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "TaskDTO")
            @RequestBody final List<TaskDTO> TasksDTO
    ) {
        TaskService.remove(UserUtil.getUserId(), TasksDTO);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        TaskService.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        TaskService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return TaskService.count(UserUtil.getUserId());
    }

}
