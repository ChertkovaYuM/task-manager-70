package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;
import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<TaskDTO, String> {

    @NotNull
    @Query("FROM TaskDTO WHERE userId = :userId AND projectId = :projectId")
    List<TaskDTO> findAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("DELETE FROM TaskDTO WHERE userId = :userId AND projectId = :projectId")
    void removeAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Modifying
    @Transactional
    @Query("DELETE FROM TaskDTO WHERE userId = :userId AND projectId IN (:projects)")
    void removeAllByProjectList(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projects") String[] projects
    );

    @NotNull
    List<TaskDTO> findAll(@NotNull Sort sort);

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable String userId, @NotNull Sort sort);

    @Modifying
    @Transactional
    void deleteByUserId(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAllByUserId(@Nullable String userId);

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    TaskDTO findFirstByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);

}
