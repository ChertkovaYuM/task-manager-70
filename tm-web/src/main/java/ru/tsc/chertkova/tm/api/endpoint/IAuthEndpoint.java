package ru.tsc.chertkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.chertkova.tm.model.dto.Result;
import ru.tsc.chertkova.tm.model.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    @PostMapping(value = "/login")
    Result login(
            @WebParam(name = "login") final String login,
            @WebParam(name = "password") final String password
    );

    @NotNull
    @WebMethod
    @GetMapping(value = "/profile")
    UserDTO profile();

    @NotNull
    @WebMethod
    @PostMapping(value = "/logout")
    Result logout();

}
