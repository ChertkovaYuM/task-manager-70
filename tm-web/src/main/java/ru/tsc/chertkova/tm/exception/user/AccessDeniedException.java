package ru.tsc.chertkova.tm.exception.user;

import ru.tsc.chertkova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Ошибка! Доступ запрещён.");
    }

}
