package ru.tsc.chertkova.tm.exception;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Ошибка! Проект не найден!");
    }

}
