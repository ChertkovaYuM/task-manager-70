package ru.tsc.chertkova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public final class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
