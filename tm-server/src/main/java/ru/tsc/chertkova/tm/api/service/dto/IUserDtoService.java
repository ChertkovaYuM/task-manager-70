package ru.tsc.chertkova.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserDtoService extends IDtoService<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    UserDTO setPassword(@Nullable String id,
                     @Nullable String password);

    @Nullable
    UserDTO updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName);

    UserDTO lockUserByLogin(@Nullable String login);

    UserDTO unlockUserByLogin(@Nullable String login);

    @Nullable
    UserDTO add(@Nullable UserDTO user);

    @Nullable
    UserDTO updateById(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String middleName,
                    @Nullable String lastName);

    boolean existsById(@Nullable String id);

    @Nullable
    UserDTO findById(@Nullable String id);

    UserDTO removeById(@Nullable String id);

    UserDTO remove(@Nullable UserDTO user);

    int getSize(@Nullable String id);

    void clear(@Nullable String id);

    @Nullable
    List<UserDTO> findAll(@Nullable String id);

    @Nullable
    List<UserDTO> addAll(@Nullable List<UserDTO> users);

    @Nullable
    List<UserDTO> removeAll(@Nullable List<UserDTO> users);

}
